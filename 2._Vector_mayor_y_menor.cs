using System;
//parametros por referencia (ref)
//crear un metodo que pase dos valores y los develva intercambiado

// Crear un programa que permita crear, cargar y obtener el menor y mayor valor de un vector. La obtención del mayor y menor hacerlo en un único método que retorne dichos dos valores.

class MainClass 
{
  class Program
  {
    private int[] vector;

    public Program()
    {
      Console.WriteLine("Ingrese tamaño del vector: ");
      int tam= int.Parse(Console.ReadLine());
      vector = new int[tam];
    }

    public void Cargar()
    {
      for (int i = 0; i < vector.Length; i++)
      {
        Console.Write("Ingrese valor: ");
        vector[i] = int.Parse(Console.ReadLine());
      }
    }
    
    public void MayorMenor(out int may, out int men)
    {
      may= vector[0];
      men= vector[0];
      for (int i = 0; i < vector.Length; i++)
      {
        if (vector[i]>may)
        {
          may= vector[i];
        }
        else
        {
          if (vector[i] <men)
          {
            men= vector[i];
          }
        }
        
      }
    }

    public static void Main (string[] args) 
    {
      Program p= new Program();

      p.Cargar();
      int mayor, menor;

      p.MayorMenor(out mayor, out menor);

      Console.WriteLine("El mayor es {0} y el menor es {1}" ,mayor,menor);
      Console.ReadKey(); 
    }
  }
  
}