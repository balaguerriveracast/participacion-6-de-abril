using System;
//parametros por referencia (ref)
//crear un metodo que pase dos valores y los develva intercambiado

//Crear un metodo que retorne 5 valores random entre 1 y 30 mediante parametros por referencia

class MainClass 
{
  class Program
  {
    public void CargarValor(out int v1,out int v2,out int v3, out int v4, out int v5)
    {
      Random rnd = new Random();
      v1= rnd.Next(1,30);
      v2= rnd.Next(1,30);
      v3= rnd.Next(1,30);
      v4= rnd.Next(1,30);
      v5= rnd.Next(1,30);
    } 

    
    public static void Main (string[] args) 
    {
      int va1, va2, va3, va4, va5;
      Program p= new Program();
      p.CargarValor(out va1,out va2,out va3,out va4,out va5);
      Console.WriteLine("Primer Aleatorio {0}" , va1);
      Console.WriteLine("Segundo Aleatorio {0}", va2);
      Console.WriteLine("Tercer Aleatorio {0}", va3);
      Console.WriteLine("Cuarto Aleatorio {0}", va4);
      Console.WriteLine("Quinto Aleatorio {0}", va5);
      Console.ReadKey();
      
    }
  }
  
}